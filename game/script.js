const cells = document.querySelectorAll(".cell");
const message = document.querySelector(".message");
const playerXScore = document.querySelector(".player-X-score");
const playerOScore = document.querySelector(".player-O-score");
const resetButton = document.querySelector(".reset-button");
const nextRoundButton = document.querySelector(".next-round-button");

let currentPlayer = "X";
let gameIsLive = true;
let winner = null;
let playerXWins = 0;
let playerOWins = 0;

const winningConditions = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

function handleCellClick(e) {
  let cell = e.target;
  if (cell.classList.contains("occupied") || !gameIsLive) {
    return;
  }
  cell.classList.add("occupied", `player-${currentPlayer}`);
  checkWin();
  if (winner) {
    endGame();
    return;
  }
  switchPlayer();
  checkDraw();
}

function checkWin() {
  for (let i = 0; i < winningConditions.length; i++) {
    let [a, b, c] = winningConditions[i];
    if (
      cells[a].classList.contains(`player-${currentPlayer}`) &&
      cells[b].classList.contains(`player-${currentPlayer}`) &&
      cells[c].classList.contains(`player-${currentPlayer}`)
    ) {
      cells[a].classList.add("win");
      cells[b].classList.add("win");
      cells[c].classList.add("win");
      winner = currentPlayer;
      updateScore();
      return;
    }
  }
}

function checkDraw() {
  let draw = true;
  for (let i = 0; i < cells.length; i++) {
    if (!cells[i].classList.contains("occupied")) {
      draw = false;
    }
  }
  if (draw) {
    nextRoundButton.classList.remove("hide");
    message.innerText = "It's a draw!";
  }
}

function switchPlayer() {
  if (currentPlayer === "X") {
    currentPlayer = "O";
    message.innerText = "Player O's Turn";
  } else {
    currentPlayer = "X";
    message.innerText = "Player X's Turn";
  }
}

function updateScore() {
  if (winner === "X") {
    playerXWins++;
  } else if (winner === "O") {
    playerOWins++;
  }
  playerXScore.textContent = `Player X: ${playerXWins}`;
  playerOScore.textContent = `Player O: ${playerOWins}`;
}

function resetBoard() {
  for (let i = 0; i < cells.length; i++) {
    cells[i].classList.remove("occupied", "player-X", "player-O", "win");
  }
  winner = null;
  gameIsLive = true;
}

function resetGame() {
  resetBoard();
  playerXWins = 0;
  playerOWins = 0;
  playerXScore.innerText = "Player X: 0";
  playerOScore.innerText = "Player O: 0";
  message.innerText = "Player X's Turn";
  nextRoundButton.classList.add("hide");
}

function endGame() {
  gameIsLive = false;
  message.classList.add("win");
  if (winner) {
    message.textContent = `Player ${winner} won the round!`;
    nextRoundButton.classList.remove("hide");
  }
}

function startNextRound() {
  winner = null;
  message.innerText = `Player ${currentPlayer}'s turn`;
  nextRoundButton.classList.add("hide");
  gameIsLive = true;
  for (let i = 0; i < cells.length; i++) {
    cells[i].classList.remove("occupied", "player-X", "player-O", "win");
  }
}

for (let i = 0; i < cells.length; i++) {
  cells[i].addEventListener("click", handleCellClick);
}

resetButton.addEventListener("click", resetGame);

nextRoundButton.addEventListener("click", startNextRound);

message.innerText = `Player ${currentPlayer}'s turn`;
message.classList.add("player-X-turn");
