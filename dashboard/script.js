let selectedCountry = null;
const countrySelect = document.getElementById("countrySelect");
const nameOfSelectedCountry = document.getElementById("nameOfCountry");
const openWeatherAPIKey = `4e4c3ebdbcfa2f82f56142ac3c489c48`;
const unsplashAPIKey = `WOEdPMZxYBwxEfvUeSYLjlWD_ZYMwBub6t3FCbmiHJk`;
const holidayPapiAPIKey = `390af53c-67c4-4966-b19f-58795fc5584a`;
const weekday = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];
const monthsFullName = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

fetch("https://restcountries.com/v3.1/all")
  .then((response) => response.json())
  .then((data) => {
    const countryNames = data.map((country) => country.name.common);
    countryNames.sort();
    for (let name of countryNames) {
      let option = document.createElement("option");
      option.value = name;
      option.textContent = name;
      countrySelect.appendChild(option);
    }
  })
  .catch((error) => console.log(error));

countrySelect.addEventListener("change", () => {
  selectedCountry = countrySelect.value;
  fetchCountryData(selectedCountry);
  fetchImagesAndSetBackground(selectedCountry);
  document.querySelector(".container").classList.remove("hidden");
});

function fetchCountryData(selectedCountry) {
  fetch(`https://restcountries.com/v3.1/name/${selectedCountry}`)
    .then((response) => response.json())
    .then((data) => {
      let countryDetails = data[0];
      let name = countryDetails.name.common;
      let fullName = countryDetails.name.official;
      let currency = Object.values(countryDetails.currencies)[0].name;
      let capital = countryDetails.capital;
      let flag = countryDetails.flags.png;
      let population = countryDetails.population;
      let area = countryDetails.area;
      let language = Object.values(countryDetails.languages)[0];
      let lat = countryDetails.latlng[0];
      let lng = countryDetails.latlng[1];
      let cca2 = countryDetails.cca2;
      let countryName = document.getElementById("country-name");
      let countryFlag = document.getElementById("country-flag");
      let countryFullName = document.getElementById("country-full-name");
      let countryCapital = document.getElementById("country-capital");
      let countryCurrency = document.getElementById("country-currency");
      let countryLanguage = document.getElementById("country-language");
      let countryPopulation = document.getElementById("country-population");
      let countryArea = document.getElementById("country-area");

      countryName.textContent = name;
      countryFlag.src = flag;
      countryFlag.alt = `Flag of ${name}`;
      countryFullName.textContent = fullName;
      countryCapital.textContent = capital;
      countryCurrency.textContent = currency;
      countryLanguage.textContent = language;
      countryPopulation.textContent = population;
      countryArea.textContent = `${area} (km²)`;

      thisMonthsHolidays(cca2);
      weatherAPI(lat, lng, name);
    })
    .catch((error) => console.log(error));
}

function weatherAPI(lat, lng, name) {
  fetch(
    `https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lng}&appid=${openWeatherAPIKey}&units=metric`
  )
    .then((response) => response.json())
    .then((data) => {
      const nameOfCountry = document.getElementById("nameOfCountry");
      const dataTime = document.getElementById("data-time");
      const weatherData = document.getElementById("weather-data");
      nameOfSelectedCountry.textContent = name;
      dataTime.textContent = new Date(data.list[0].dt * 1000).toLocaleString();
      weatherData.innerHTML = "";
      for (let i = 0; i < 5; i++) {
        let weather = data.list[i * 8];
        let date = new Date(weather.dt * 1000);
        let dayOfWeek = weekday[date.getDay()];
        let month = months[date.getMonth()];
        let dayOfMonth = date.getDate();

        const temp = weather.main.temp.toFixed(1);
        const humidity = weather.main.humidity;
        const main = weather.weather[0].main;
        const description = weather.weather[0].description;
        const iconUrl = `https://openweathermap.org/img/w/${weather.weather[0].icon}.png`;

        const row = document.createElement("tr");
        row.innerHTML = `
        <td>${dayOfWeek} ${month} ${dayOfMonth}</td>
        <td>${temp}</td>
        <td>${humidity}</td>
        <td>${main}</td>
        <td>${description}</td>
        <td><img src="${iconUrl}" alt="${description}"></td>
      `;
        weatherData.appendChild(row);
      }
    })
    .catch((error) => console.log(error));
}

function fetchImagesAndSetBackground(query) {
  let url = `https://api.unsplash.com/search/photos?query=${query}&per_page=10&client_id=${unsplashAPIKey}`;
  return fetch(url)
    .then((response) => response.json())
    .then((data) => {
      imageUrls = data.results.map((result) => result.urls.regular);
      let j = 0;
      setInterval(() => {
        document.body.style.backgroundImage = `url('${imageUrls[j]}')`;
        j = (j + 1) % imageUrls.length;
      }, 20000);
    })
    .catch((error) => console.log(error));
}

function thisMonthsHolidays(country) {
  let now = new Date();
  let month = now.getMonth() + 1;
  fetch(
    `https://holidayapi.com/v1/holidays?key=${holidayPapiAPIKey}&country=${country}&year=2022&month=${month}`
  )
    .then((response) => response.json())
    .then((data) => {
      let holidays = data.holidays;
      let holidayList = document.getElementById("holiday-list");
      let holidayHeader = document.getElementById("holiday-header");
      holidayHeader.textContent = `Holidays of ${monthsFullName[month - 1]}`;
      holidayList.innerHTML = "";

      for (let holiday of holidays) {
        let li = document.createElement("li");
        li.textContent = `${holiday.date} - ${holiday.name}`;
        holidayList.appendChild(li);
      }
    })
    .catch((error) => console.log(error));
}
